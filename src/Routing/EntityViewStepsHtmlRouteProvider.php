<?php

namespace Drupal\entity_view_steps\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Entity view steps entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class EntityViewStepsHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $route_collection = parent::getRoutes($entity_type);

    $route = (new Route('/entity-view-steps/{entity_view_steps}/{step}'))
      ->addDefaults([
        '_controller' => '\Drupal\entity_view_steps\Controller\EntityViewStepsViewController::viewStep',
        '_title_callback' => '\Drupal\entity_view_steps\Controller\EntityViewStepsViewController::title',
      ])
      ->setRequirement('entity_view_steps', '\d+')
      ->setRequirement('step', '\d+')
      ->setRequirement('_entity_access', 'entity_view_steps.view');
    $route_collection->add('entity.entity_view_steps.step.canonical', $route);

    $route = (new Route('/entity-view-steps/{entity_view_steps}/delete'))
      ->addDefaults([
        '_entity_form' => "entity_view_steps.delete",
        '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::deleteTitle',
      ])
      ->setRequirement('entity_view_steps', '\d+')
      ->setRequirement('_entity_access', 'entity_view_steps.delete')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.entity_view_steps.delete_form', $route);

    $route = (new Route('/entity-view-steps/{entity_view_steps}/edit'))
      ->setDefault('_entity_form', 'entity_view_steps.edit')
      ->setRequirement('_entity_access', 'entity_view_steps.update')
      ->setRequirement('entity_view_steps', '\d+')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.entity_view_steps.edit_form', $route);

    return $route_collection;
  }

}
