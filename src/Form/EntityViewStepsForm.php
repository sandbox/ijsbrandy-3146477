<?php

namespace Drupal\entity_view_steps\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Entity view steps edit forms.
 *
 * @ingroup entity_view_steps
 */
class EntityViewStepsForm extends ContentEntityForm {

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, AccountInterface $current_user, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

//  /**
//   * {@inheritdoc}
//   */
//  public function buildForm(array $form, FormStateInterface $form_state) {
//    $form = parent::buildForm($form, $form_state);
//
//    if (!$this->entity->isNew()) {
//      $form['new_revision'] = [
//        '#type' => 'checkbox',
//        '#title' => $this->t('Create new revision'),
//        '#default_value' => FALSE,
//        '#weight' => 10,
//      ];
//    }
//    /** @var \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity_view_steps */
//    $entity_view_steps = $this->entity;
//
//    if ($this->operation == 'edit') {
//      $form['#title'] = $this->t('<em>Edit @type</em> @title', [
//        '@type' => $entity_view_steps->type->entity->label(),
//        '@title' => $entity_view_steps->label(),
//      ]);
//    }
//
//    // Changed must be sent to the client, for later overwrite error checking.
//    $form['changed'] = [
//      '#type' => 'hidden',
//      '#default_value' => $entity_view_steps->getChangedTime(),
//    ];
//
//    $form = parent::form($form, $form_state);
//
//    $form['advanced']['#attributes']['class'][] = 'entity-meta';
//
//    $form['meta'] = [
//      '#type' => 'details',
//      '#group' => 'advanced',
//      '#weight' => -10,
//      '#title' => $this->t('Status'),
//      '#attributes' => ['class' => ['entity-meta__header']],
//      '#tree' => TRUE,
//    ];
//    $form['meta']['published'] = [
//      '#type' => 'item',
//      '#markup' => $entity_view_steps->isPublished() ? $this->t('Published') : $this->t('Not published'),
//      '#access' => !$entity_view_steps->isNew(),
//      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
//    ];
//    $form['meta']['changed'] = [
//      '#type' => 'item',
//      '#title' => $this->t('Last saved'),
//      '#markup' => !$entity_view_steps->isNew() ? $this->dateFormatter->format($entity_view_steps->getChangedTime(), 'short') : $this->t('Not saved yet'),
//      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
//    ];
//    $form['meta']['author'] = [
//      '#type' => 'item',
//      '#title' => $this->t('Author'),
//      '#markup' => $entity_view_steps->getOwner()->getAccountName(),
//      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
//    ];
//
//    $form['status']['#group'] = 'footer';
//
//    // Node author information for administrators.
//    $form['author'] = [
//      '#type' => 'details',
//      '#title' => t('Authoring information'),
//      '#group' => 'advanced',
//      '#attributes' => [
//        'class' => ['node-form-author'],
//      ],
//      '#attached' => [
//        'library' => ['node/drupal.node'],
//      ],
//      '#weight' => 90,
//      '#optional' => TRUE,
//    ];
//
//    if (isset($form['user_id'])) {
//      $form['user_id']['#group'] = 'author';
//    }
//
//    if (isset($form['created'])) {
//      $form['created']['#group'] = 'author';
//    }
//
//    $form['#attached']['library'][] = 'node/form';
//
//    return $form;
//  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity_view_steps */
    $entity_view_steps = $this->entity;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit @type</em> @title', [
        '@type' => $entity_view_steps->type->entity->label(),
        '@title' => $entity_view_steps->label(),
      ]);
    }

    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = [
      '#type' => 'hidden',
      '#default_value' => $entity_view_steps->getChangedTime(),
    ];

    $form = parent::form($form, $form_state);
    $form['#theme'] = ['entity_view_steps_edit_form'];

    $form['advanced']['#attributes']['class'][] = 'entity-meta';

    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => $this->currentUser->hasPermission('administer nodes'),
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $entity_view_steps->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$entity_view_steps->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$entity_view_steps->isNew() ? $this->dateFormatter->format($entity_view_steps->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $entity_view_steps->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
    ];

    $form['status']['#group'] = 'footer';

    // Node author information for administrators.
    $form['author'] = [
      '#type' => 'details',
      '#title' => t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['node-form-author'],
      ],
      '#attached' => [
        'library' => ['node/drupal.node'],
      ],
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    if (isset($form['user_id'])) {
      $form['user_id']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    $form['advanced']['#type'] = 'container';
    $form['meta']['#type'] = 'container';
    $form['meta']['#access'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Entity view steps.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Entity view steps.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.entity_view_steps.canonical', ['entity_view_steps' => $entity->id()]);
  }

}
