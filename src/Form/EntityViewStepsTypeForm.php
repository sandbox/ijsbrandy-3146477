<?php

namespace Drupal\entity_view_steps\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityViewStepsTypeForm.
 */
class EntityViewStepsTypeForm extends EntityForm {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a EntityViewStepsTypeForm
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\entity_view_steps\Entity\EntityViewStepsTypeInterface $entity_view_steps_type */
    $entity_view_steps_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity_view_steps_type->label(),
      '#description' => $this->t("Label for the Entity view steps type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_view_steps_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\entity_view_steps\Entity\EntityViewStepsType::load',
      ],
      '#disabled' => !$entity_view_steps_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $entity_view_steps_type->getDescription(),
      '#description' => t('Describe this entity view steps. The text will be displayed on the <em>entity view steps</em> administration overview page.'),
      '#title' => t('Description'),
    ];

    if ($entity_view_steps_type->isNew()) {
      $options = [];
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
        if ($entity_type->get('field_ui_base_route')) {
          $options[$entity_type->id()] = $entity_type->getLabel();
        }
      }
      $form['target_entity_type_id'] = [
        '#type' => 'select',
        '#default_value' => $entity_view_steps_type->getTargetEntityTypeId(),
        '#title' => t('Target entity type'),
        '#options' => $options,
        '#description' => t('The target entity type can not be changed after the entity view steps type has been created.'),
      ];
    }
    else {
      $form['target_entity_type_id_display'] = [
        '#type' => 'item',
        '#markup' => $this->entityTypeManager->getDefinition($entity_view_steps_type->getTargetEntityTypeId())->getLabel(),
        '#title' => t('Target entity type'),
      ];
    }

    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => t('Create new revision'),
      '#default_value' => $entity_view_steps_type->shouldCreateNewRevision()
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity_view_steps_type = $this->entity;
    $status = $entity_view_steps_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Entity view steps type.', [
          '%label' => $entity_view_steps_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Entity view steps type.', [
          '%label' => $entity_view_steps_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($entity_view_steps_type->toUrl('collection'));
  }

}
