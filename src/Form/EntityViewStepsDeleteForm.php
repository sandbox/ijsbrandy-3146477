<?php

namespace Drupal\entity_view_steps\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entity view steps entities.
 *
 * @ingroup entity_view_steps
 */
class EntityViewStepsDeleteForm extends ContentEntityDeleteForm {


}
