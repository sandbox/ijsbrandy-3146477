<?php

namespace Drupal\entity_view_steps;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_view_steps\Entity\EntityViewStepsInterface;

/**
 * Defines the storage handler class for Entity view steps entities.
 *
 * This extends the base storage class, adding required special handling for
 * Entity view steps entities.
 *
 * @ingroup entity_view_steps
 */
class EntityViewStepsStorage extends SqlContentEntityStorage implements EntityViewStepsStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(EntityViewStepsInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {entity_view_steps_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {entity_view_steps_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(EntityViewStepsInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {entity_view_steps_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('entity_view_steps_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
