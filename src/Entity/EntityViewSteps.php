<?php

namespace Drupal\entity_view_steps\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Entity view steps entity.
 *
 * @ingroup entity_view_steps
 *
 * @ContentEntityType(
 *   id = "entity_view_steps",
 *   label = @Translation("Entity view steps"),
 *   bundle_label = @Translation("Entity view steps type"),
 *   handlers = {
 *     "storage" = "Drupal\entity_view_steps\EntityViewStepsStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_view_steps\EntityViewStepsListBuilder",
 *     "views_data" = "Drupal\entity_view_steps\Entity\EntityViewStepsViewsData",
 *     "translation" = "Drupal\entity_view_steps\EntityViewStepsTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\entity_view_steps\Form\EntityViewStepsForm",
 *       "add" = "Drupal\entity_view_steps\Form\EntityViewStepsForm",
 *       "edit" = "Drupal\entity_view_steps\Form\EntityViewStepsForm",
 *       "delete" = "Drupal\entity_view_steps\Form\EntityViewStepsDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity_view_steps\Routing\EntityViewStepsHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\entity_view_steps\EntityViewStepsAccessControlHandler",
 *   },
 *   base_table = "entity_view_steps",
 *   data_table = "entity_view_steps_field_data",
 *   revision_table = "entity_view_steps_revision",
 *   revision_data_table = "entity_view_steps_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer entity view steps entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/entity-view-steps/{entity_view_steps}",
 *     "add-page" = "/admin/entity-view-steps/add",
 *     "add-form" = "/admin/entity-view-steps/add/{entity_view_steps_type}",
 *     "edit-form" = "/admin/entity-view-steps/{entity_view_steps}/edit",
 *     "delete-form" = "/admin/entity-view-steps/{entity_view_steps}/delete",
 *     "version-history" = "/admin/entity-view-steps/{entity_view_steps}/revisions",
 *     "revision" = "/admin/entity-view-steps/{entity_view_steps}/revisions/{entity_view_steps_revision}/view",
 *     "revision_revert" = "/admin/entity-view-steps/{entity_view_steps}/revisions/{entity_view_steps_revision}/revert",
 *     "revision_delete" = "/admin/entity-view-steps/{entity_view_steps}/revisions/{entity_view_steps_revision}/delete",
 *     "translation_revert" = "/admin/entity-view-steps/{entity_view_steps}/revisions/{entity_view_steps_revision}/revert/{langcode}",
 *     "collection" = "/admin/entity-view-steps",
 *   },
 *   bundle_entity_type = "entity_view_steps_type",
 *   field_ui_base_route = "entity.entity_view_steps_type.edit_form"
 * )
 */
class EntityViewSteps extends EditorialContentEntityBase implements EntityViewStepsInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the entity_view_steps owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    return $this->get('steps')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function addStepId($step_id) {
    $this->get('steps')->appendItem([
      'target_id' => $step_id,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Entity view steps entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Entity view steps entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['steps'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Content steps'))
      ->setDescription(t('The content to display as step.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 0,
      ])
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Entity view steps is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    if ($entity_view_steps_type = EntityViewStepsType::load($bundle)) {
      $fields['steps'] = clone $base_field_definitions['steps'];
      $fields['steps']->setSetting('target_type', $entity_view_steps_type->getTargetEntityTypeId());
      return $fields;
    }
    return [];
  }

}
