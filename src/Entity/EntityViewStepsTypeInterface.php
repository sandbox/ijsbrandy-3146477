<?php

namespace Drupal\entity_view_steps\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface for defining Entity view steps type entities.
 */
interface EntityViewStepsTypeInterface extends ConfigEntityInterface, RevisionableEntityBundleInterface {

  /**
   * Returns the entity view steps type description.
   *
   * @return string
   *   The entity view steps type description.
   */
  public function getDescription();

  /**
   * Sets the description of the entity view steps type.
   *
   * @param string $description
   *   The new description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the target entity type id for this entity view steps type .
   *
   * @return string
   *   The target entity type id.
   */
  public function getTargetEntityTypeId();
}
