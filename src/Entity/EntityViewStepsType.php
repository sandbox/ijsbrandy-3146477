<?php

namespace Drupal\entity_view_steps\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Entity view steps type entity.
 *
 * @ConfigEntityType(
 *   id = "entity_view_steps_type",
 *   label = @Translation("Entity view steps type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_view_steps\EntityViewStepsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_view_steps\Form\EntityViewStepsTypeForm",
 *       "edit" = "Drupal\entity_view_steps\Form\EntityViewStepsTypeForm",
 *       "delete" = "Drupal\entity_view_steps\Form\EntityViewStepsTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity_view_steps\Routing\EntityViewStepsTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "entity_view_steps_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "entity_view_steps",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/entity_view_steps_type/{entity_view_steps_type}",
 *     "add-form" = "/admin/structure/entity_view_steps_type/add",
 *     "edit-form" = "/admin/structure/entity_view_steps_type/{entity_view_steps_type}/edit",
 *     "delete-form" = "/admin/structure/entity_view_steps_type/{entity_view_steps_type}/delete",
 *     "collection" = "/admin/structure/entity_view_steps_type"
 *   }
 * )
 */
class EntityViewStepsType extends ConfigEntityBundleBase implements EntityViewStepsTypeInterface {

  /**
   * The Entity view steps type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Entity view steps type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the Entity view steps type.
   *
   * @var string
   */
  protected $description;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * Default value of the 'Create new revision' checkbox of this Entity view steps type.
   *
   * @var bool
   */
  protected $new_revision = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

}
