<?php

namespace Drupal\entity_view_steps\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Entity view steps entities.
 *
 * @ingroup entity_view_steps
 */
interface EntityViewStepsInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Entity view steps name.
   *
   * @return string
   *   Name of the Entity view steps.
   */
  public function getName();

  /**
   * Sets the Entity view steps name.
   *
   * @param string $name
   *   The Entity view steps name.
   *
   * @return \Drupal\entity_view_steps\Entity\EntityViewStepsInterface
   *   The called Entity view steps entity.
   */
  public function setName($name);

  /**
   * Gets the Entity view steps creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entity view steps.
   */
  public function getCreatedTime();

  /**
   * Sets the Entity view steps creation timestamp.
   *
   * @param int $timestamp
   *   The Entity view steps creation timestamp.
   *
   * @return \Drupal\entity_view_steps\Entity\EntityViewStepsInterface
   *   The called Entity view steps entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Entity view steps revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Entity view steps revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\entity_view_steps\Entity\EntityViewStepsInterface
   *   The called Entity view steps entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Entity view steps revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Entity view steps revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\entity_view_steps\Entity\EntityViewStepsInterface
   *   The called Entity view steps entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Gets the Entity view steps steps.
   *
   * @return array
   *   The entity steps
   */
  public function getSteps();

  /**
   * Adds Entity step id.
   *
   * @param int $step_id
   *   The entity id.
   *
   * @return \Drupal\entity_view_steps\Entity\EntityViewStepsInterface
   *   The called Entity view steps entity.
   */
  public function addStepId($step_id);

}
