<?php

namespace Drupal\entity_view_steps;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for entity_view_steps.
 */
class EntityViewStepsTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
