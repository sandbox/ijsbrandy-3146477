<?php

namespace Drupal\entity_view_steps\Service;

use Drupal\entity_view_steps\Entity\EntityViewStepsInterface;

/**
 * Class ProgressBarService
 *
 * @package Drupal\entity_view_steps\Services
 */
class ProgressBarService {

  /**
   * @param \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity_view_steps
   * @param mixed $current_step
   *
   * @return array
   */
  public function build(EntityViewStepsInterface $entity_view_steps, $current_step = NULL) {
    $steps = $entity_view_steps->get('steps')->getValue();

    $position = NULL;
    if ($current_step) {
      $position = array_search($current_step->id(), array_column($steps, 'target_id'));
      if ($position === FALSE) {
        return [];
      }
    }

    return [
      '#theme' => 'entity_view_steps_progress_bar',
      '#entity_view_steps' => $entity_view_steps,
      '#steps' => $entity_view_steps->get('steps')->referencedEntities(),
      '#current_step' => $current_step,
      '#current_position' => ($position !== NULL) ? $position+ 1 : 0,
    ];
  }
}
