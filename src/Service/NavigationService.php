<?php

namespace Drupal\entity_view_steps\Service;

use Drupal\entity_view_steps\Entity\EntityViewStepsInterface;

/**
 * Class NavigationService
 *
 * @package Drupal\entity_view_steps\Service
 */
class NavigationService {

  /**
   * @param \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity_view_steps
   * @param mixed $current_step
   *
   * @return array
   */
  public function build(EntityViewStepsInterface $entity_view_steps, $current_step = NULL) {
    $steps = $entity_view_steps->get('steps')->getValue();

    $position = NULL;
    if ($current_step) {
      $position = array_search($current_step->id(), array_column($steps, 'target_id'));
      if ($position === FALSE) {
        return [];
      }
    }

    $step_entities = $entity_view_steps->get('steps')->referencedEntities();

    return [
      '#theme' => 'entity_view_steps_navigation',
      '#entity_view_steps' => $entity_view_steps,
      '#previous' => $step_entities[$position - 1] ?? NULL,
      '#next' => $step_entities[$position + 1] ?? NULL,
    ];
  }

}
