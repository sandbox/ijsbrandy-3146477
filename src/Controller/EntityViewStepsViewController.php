<?php

namespace Drupal\entity_view_steps\Controller;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class EntityViewStepsViewController
 *
 * @package Drupal\entity_view_steps\Controller
 */
class EntityViewStepsViewController extends EntityViewController {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Creates an NodeViewController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user. For backwards compatibility this is optional, however
   *   this will be removed before Drupal 9.0.0.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, DateFormatter $date_formatter, AccountInterface $current_user = NULL, EntityRepositoryInterface $entity_repository = NULL) {
    parent::__construct($entity_type_manager, $renderer);
    $this->dateFormatter = $date_formatter;
    $this->currentUser = $current_user ?: \Drupal::currentUser();
    if (!$entity_repository) {
      @trigger_error('The entity.repository service must be passed to NodeViewController::__construct(), it is required before Drupal 9.0.0. See https://www.drupal.org/node/2549139.', E_USER_DEPRECATED);
      $entity_repository = \Drupal::service('entity.repository');
    }
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('date.formatter'),
      $container->get('current_user'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewStep(EntityInterface $entity_view_steps, $step, $view_mode = 'full') {
    $steps = $entity_view_steps->get('steps')->getValue();

    if (array_search($step, array_column($steps, 'target_id')) === FALSE) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\entity_view_steps\Entity\EntityViewStepsTypeInterface $entity_view_steps_type */
    $entity_view_steps_type = $entity_view_steps->type->entity;

    $step_entity = $this->entityTypeManager
      ->getStorage($entity_view_steps_type->getTargetEntityTypeId())->load($step);

    $build = $this->entityTypeManager
      ->getViewBuilder($step_entity->getEntityTypeId())
      ->view($step_entity, $view_mode);

    $build['#theme'] = 'entity_view_steps_step';
    $build['#pre_render'][] = [$this, 'buildTitle'];
    $build['#entity_type'] = $step_entity->getEntityTypeId();

    $build['#' . $entity_view_steps->getEntityTypeId()] = $entity_view_steps;

    $build['#progress_bar'] = \Drupal::service('entity_view_steps.progress_bar')->build(
      $entity_view_steps,
      $step_entity
    );

    $build['#navigation'] = \Drupal::service('entity_view_steps.navigation')->build(
      $entity_view_steps,
      $step_entity
    );

    foreach ($entity_view_steps->uriRelationships() as $rel) {
      $url = $entity_view_steps->toUrl($rel)->setAbsolute(TRUE);
      if ($this->currentUser->isAuthenticated() || $url->access($this->currentUser)) {
        // Set the node path as the canonical URL to prevent duplicate content.
        $build['#attached']['html_head_link'][] = [
          [
            'rel' => $rel,
            'href' => $url->toString(),
          ],
          TRUE,
        ];
      }

      if ($rel == 'canonical') {
        // Set the non-aliased canonical path as a default shortlink.
        $build['#attached']['html_head_link'][] = [
          [
            'rel' => 'shortlink',
            'href' => $url->setOption('alias', TRUE)->toString(),
          ],
          TRUE,
        ];
      }
    }

    // Since this generates absolute URLs, it can only be cached "per site".
    $build['#cache']['contexts'][] = 'url.site';

    // Given this varies by $this->currentUser->isAuthenticated(), add a cache
    // context based on the anonymous role.
    $build['#cache']['contexts'][] = 'user.roles:anonymous';

    return $build;
  }

}

