<?php

namespace Drupal\entity_view_steps\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\entity_view_steps\Entity\EntityViewStepsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityViewStepsController.
 *
 *  Returns responses for Entity view steps routes.
 */
class EntityViewStepsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new EntityViewStepsController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Entity view steps revision.
   *
   * @param int $entity_view_steps_revision
   *   The Entity view steps revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($entity_view_steps_revision) {
    $entity_view_steps = $this->entityTypeManager()->getStorage('entity_view_steps')
      ->loadRevision($entity_view_steps_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('entity_view_steps');

    return $view_builder->view($entity_view_steps);
  }

  /**
   * Page title callback for a Entity view steps revision.
   *
   * @param int $entity_view_steps_revision
   *   The Entity view steps revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($entity_view_steps_revision) {
    $entity_view_steps = $this->entityTypeManager()->getStorage('entity_view_steps')
      ->loadRevision($entity_view_steps_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $entity_view_steps->label(),
      '%date' => $this->dateFormatter->format($entity_view_steps->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Entity view steps.
   *
   * @param \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity_view_steps
   *   A Entity view steps object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(EntityViewStepsInterface $entity_view_steps) {
    $account = $this->currentUser();
    $entity_view_steps_storage = $this->entityTypeManager()->getStorage('entity_view_steps');

    $langcode = $entity_view_steps->language()->getId();
    $langname = $entity_view_steps->language()->getName();
    $languages = $entity_view_steps->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $entity_view_steps->label()]) : $this->t('Revisions for %title', ['%title' => $entity_view_steps->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all entity view steps revisions") || $account->hasPermission('administer entity view steps entities')));
    $delete_permission = (($account->hasPermission("delete all entity view steps revisions") || $account->hasPermission('administer entity view steps entities')));

    $rows = [];

    $vids = $entity_view_steps_storage->revisionIds($entity_view_steps);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\entity_view_steps\EntityViewStepsInterface $revision */
      $revision = $entity_view_steps_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $entity_view_steps->getRevisionId()) {
          $link = $this->l($date, new Url('entity.entity_view_steps.revision', [
            'entity_view_steps' => $entity_view_steps->id(),
            'entity_view_steps_revision' => $vid,
          ]));
        }
        else {
          $link = $entity_view_steps->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.entity_view_steps.translation_revert', [
                'entity_view_steps' => $entity_view_steps->id(),
                'entity_view_steps_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity_view_steps.revision_revert_confirm', [
                'entity_view_steps' => $entity_view_steps->id(),
                'entity_view_steps_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity_view_steps.revision_delete_confirm', [
                'entity_view_steps' => $entity_view_steps->id(),
                'entity_view_steps_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['entity_view_steps_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
