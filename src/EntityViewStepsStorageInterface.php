<?php

namespace Drupal\entity_view_steps;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_view_steps\Entity\EntityViewStepsInterface;

/**
 * Defines the storage handler class for Entity view steps entities.
 *
 * This extends the base storage class, adding required special handling for
 * Entity view steps entities.
 *
 * @ingroup entity_view_steps
 */
interface EntityViewStepsStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Entity view steps revision IDs for a specific Entity view steps.
   *
   * @param \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity
   *   The Entity view steps entity.
   *
   * @return int[]
   *   Entity view steps revision IDs (in ascending order).
   */
  public function revisionIds(EntityViewStepsInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Entity view steps author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Entity view steps revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\entity_view_steps\Entity\EntityViewStepsInterface $entity
   *   The Entity view steps entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(EntityViewStepsInterface $entity);

  /**
   * Unsets the language for all Entity view steps with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
